% Script for Jin's debugging
% 
% Problems in order of urgency:
% 1) sending patterns to the panels no longer works
% 2) gs 2 patterns with row compression do not display
% 3) position functions take an extra sample rate duration before starting
% 4) identity compression no longer works correctly

%% Make patterns and position functions
%==========================================================================

make_pats_pos_funcs = 1;

if make_pats_pos_funcs

    % Add panels matlab code to path
    panels_matlab_code_path = '~/XmegaController_Matlab_V13';
    addpath(genpath(panels_matlab_code_path))
    panel_id_map =                  [12  8  4 11  7  3 10  6  2  9  5  1;
                                     24 20 16 23 19 15 22 18 14 21 17 13;
                                     36 32 28 35 31 27 34 30 26 33 29 25;
                                     48 44 40 47 43 39 46 42 38 45 41 37];
    save_directory = mfilename('fullpath');
    save_directory = fullfile(fileparts(save_directory),'SD_card_contents');
    if ~exist(save_directory,'dir')
        mkdir(save_directory);
    end
% Make a gs2 pattern with row compression
    pattern.gs_val = 2;
    pattern.row_compression = 1;
    
    % Patttern values
    num_rows = 4;
    num_cols = 96;
    stripe_size = 4;
    mid_gs_value = 2;
    high_gs_value = 3;
    low_gs_value = 0;
    
    % Make Pats matrix
    pattern.Pats(:,:,1) = mid_gs_value*ones(num_rows,num_cols);
    base_pat = repmat([low_gs_value*ones(num_rows,stripe_size), high_gs_value*ones(num_rows,stripe_size)],1,num_cols/(2*stripe_size));
    for i = 1:stripe_size*2
        pattern.Pats(:,:,i+1,1) = circshift(base_pat,[0 i-1]); %#ok<*SAGROW>
    end

    pattern.x_num       = size(pattern.Pats,3);
    pattern.y_num       = size(pattern.Pats,4);
    pattern.num_panels  = 48;
    pattern.Panel_map = panel_id_map;
    pattern.BitMapIndex = process_panel_map(pattern);
    pattern.data = Make_pattern_vector(pattern);

    pattern_name = 'Pattern_01_gs2_with_row_compression';
    file_name = fullfile(save_directory,pattern_name);
    disp(file_name);
    save(file_name, 'pattern');

    clear pattern
    
% Make the same gs2 pattern without row compression
    pattern.gs_val = 2;
    pattern.row_compression = 0;
    
    % Patttern values
    num_rows = 32;
    num_cols = 96;
    stripe_size = 4;
    mid_gs_value = 2;
    high_gs_value = 3;
    low_gs_value = 0;
    
    % Make Pats matrix
    pattern.Pats(:,:,1) = mid_gs_value*ones(num_rows,num_cols);
    base_pat = repmat([low_gs_value*ones(num_rows,stripe_size), high_gs_value*ones(num_rows,stripe_size)],1,num_cols/(2*stripe_size));
    for i = 1:stripe_size*2
        pattern.Pats(:,:,i+1,1) = circshift(base_pat,[0 i-1]); %#ok<*SAGROW>
    end

    pattern.x_num       = size(pattern.Pats,3);
    pattern.y_num       = size(pattern.Pats,4);
    pattern.num_panels  = 48;
    pattern.Panel_map = panel_id_map;
    pattern.BitMapIndex = process_panel_map(pattern);
    pattern.data = Make_pattern_vector(pattern);

    pattern_name = 'Pattern_02_gs2_no_row_compression';
    file_name = fullfile(save_directory,pattern_name);
    disp(file_name);
    save(file_name, 'pattern');

    clear pattern
    
% Make a gs2 pattern of horizontal bars to show problems with identity
% compression.
    pattern.gs_val = 2;
    pattern.row_compression = 0;
    
    % Patttern values
    num_rows = 32;
    num_cols = 96;
    stripe_size = 4;
    mid_gs_value = 2;
    high_gs_value = 3;
    low_gs_value = 0;
    
    % Make Pats matrix
    pattern.Pats(:,:,1) = mid_gs_value*ones(num_rows,num_cols);
    base_pat = repmat([low_gs_value*ones(stripe_size,num_cols); high_gs_value*ones(stripe_size,num_cols)],num_rows/(2*stripe_size),1);
    for i = 1:stripe_size*2
        pattern.Pats(:,:,i+1,1) = circshift(base_pat,[-i 0]);
    end

    pattern.x_num       = size(pattern.Pats,3);
    pattern.y_num       = size(pattern.Pats,4);
    pattern.num_panels  = 48;
    pattern.Panel_map = panel_id_map;
    pattern.BitMapIndex = process_panel_map(pattern);
    pattern.data = Make_pattern_vector(pattern);

    pattern_name = 'Pattern_03_gs2_no_row_compression_broken_ident_compression';
    file_name = fullfile(save_directory,pattern_name);
    disp(file_name);
    save(file_name, 'pattern');

    clear pattern

% Make a gs3 pattern of vertical bars with row_compression
    pattern.gs_val = 3;
    pattern.row_compression = 0;
    
    % Patttern values
    num_rows = 32;
    num_cols = 96;
    stripe_size = 4;
    mid_gs_value = 3;
    high_gs_value = 7;
    low_gs_value = 0;
    
    % Make Pats matrix
    pattern.Pats(:,:,1) = mid_gs_value*ones(num_rows,num_cols);
    base_pat = repmat([low_gs_value*ones(num_rows,stripe_size), high_gs_value*ones(num_rows,stripe_size)],1,num_cols/(2*stripe_size));
    for i = 1:stripe_size*2
        pattern.Pats(:,:,i+1,1) = circshift(base_pat,[0 i-1]);
    end

    pattern.x_num       = size(pattern.Pats,3);
    pattern.y_num       = size(pattern.Pats,4);
    pattern.num_panels  = 48;
    pattern.Panel_map = panel_id_map;
    pattern.BitMapIndex = process_panel_map(pattern);
    pattern.data = Make_pattern_vector(pattern);

    pattern_name = 'Pattern_04_gs3_with_row_compression';
    file_name = fullfile(save_directory,pattern_name);
    disp(file_name);
    save(file_name, 'pattern');

    clear pattern  
    
% Make a gs3 pattern of vertical bars without row_compression
    pattern.gs_val = 3;
    pattern.row_compression = 0;
    
    % Patttern values
    num_rows = 32;
    num_cols = 96;
    stripe_size = 4;
    mid_gs_value = 3;
    high_gs_value = 7;
    low_gs_value = 0;
    
    % Make Pats matrix
    pattern.Pats(:,:,1) = mid_gs_value*ones(num_rows,num_cols);
    base_pat = repmat([low_gs_value*ones(num_rows,stripe_size), high_gs_value*ones(num_rows,stripe_size)],1,num_cols/(2*stripe_size));
    for i = 1:stripe_size*2
        pattern.Pats(:,:,i+1,1) = circshift(base_pat,[0 i-1]);
    end

    pattern.x_num       = size(pattern.Pats,3);
    pattern.y_num       = size(pattern.Pats,4);
    pattern.num_panels  = 48;
    pattern.Panel_map = panel_id_map;
    pattern.BitMapIndex = process_panel_map(pattern);
    pattern.data = Make_pattern_vector(pattern);

    pattern_name = 'Pattern_05_gs3_no_row_compression';
    file_name = fullfile(save_directory,pattern_name);
    disp(file_name);
    save(file_name, 'pattern');

    clear pattern  
    
% Make a gs3 pattern of horizontal bars
    pattern.gs_val = 3;
    pattern.row_compression = 0;
    
    % Patttern values
    num_rows = 32;
    num_cols = 96;
    stripe_size = 4;
    mid_gs_value = 3;
    high_gs_value = 7;
    low_gs_value = 0;
    
    % Make Pats matrix
    pattern.Pats(:,:,1) = mid_gs_value*ones(num_rows,num_cols);
    base_pat = repmat([low_gs_value*ones(stripe_size,num_cols); high_gs_value*ones(stripe_size,num_cols)],num_rows/(2*stripe_size),1);
    for i = 1:stripe_size*2
        pattern.Pats(:,:,i+1,1) = circshift(base_pat,[-i 0]);
    end

    pattern.x_num       = size(pattern.Pats,3);
    pattern.y_num       = size(pattern.Pats,4);
    pattern.num_panels  = 48;
    pattern.Panel_map = panel_id_map;
    pattern.BitMapIndex = process_panel_map(pattern);
    pattern.data = Make_pattern_vector(pattern);

    pattern_name = 'Pattern_06_gs3_horizontal_bars_broken_ident_compression';
    file_name = fullfile(save_directory,pattern_name);
    disp(file_name);
    save(file_name, 'pattern');

    clear pattern  
    
% Make simple position functions with enough steps for the above patterns
    
    func = 2:13;
    function_name = ['position_function_01_pat_pos_' num2str(func(1)) '_to_' num2str(func(end))];
    file_name = fullfile(save_directory,function_name);
    save(file_name, 'func');
    disp(file_name);
    
    func = 2:3;
    function_name = ['position_function_02_pat_pos_' num2str(func(1)) '_to_' num2str(func(end))];
    file_name = fullfile(save_directory,function_name);
    save(file_name, 'func');
    disp(file_name);
    
    func = 1:2;
    function_name = ['position_function_03_pat_pos_' num2str(func(1)) '_to_' num2str(func(end))];
    file_name = fullfile(save_directory,function_name);
    save(file_name, 'func');
    disp(file_name);
    
end
