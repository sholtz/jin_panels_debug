%% Script for Jin's debugging
% 
% Problems in order of urgency:
% 1) sending patterns to the panels no longer works
% 2) gs 2 patterns with row compression do not display
% 3) position functions take an extra sample rate duration before starting
% 4) identity compression no longer works correctly

% Test out the problems
%========================================================================== 
%% Try sending a pattern to the panels for fast display

% PControl; pause(5);
pat_num = 4;
Panel_com('load_pattern_2panels',pat_num); pause(.03)
Panel_com('set_position',[1 1]); pause(.03)
Panel_com('set_mode',[0 0]); pause(.03)
Panel_com('send_gain_bias',[100 0 0 0]); pause(.03)
Panel_com('start');
%Panel_com('stop');

%% Try regular pattern display
pat_num = 1;
Panel_com('set_pattern_id',pat_num); pause(.03)
Panel_com('set_position',[1 1]); pause(.03)
Panel_com('set_mode',[0 0]); pause(.03)
Panel_com('send_gain_bias',[100 0 0 0]); pause(.03)

% I do this in real experiments, it is not needed here though
Panel_com('set_ao',[3,5*(32767/10)]);

Panel_com('start');
%Panel_com('stop');

%% Try the low frequency position function
pat_num = 2;
samp_rate = 1;
pf_num = 1;

Panel_com('set_pattern_id',pat_num);pause(.03)
Panel_com('set_position',[1 1]);pause(.03)
Panel_com('set_mode',[4 0]);pause(.03)
Panel_com('send_gain_bias',[0 0 0 0]); pause(.03)

Panel_com('set_posfunc_id',[2 0]);pause(.03) % Y pos func not used
Panel_com('set_posfunc_id',[1 pf_num]);pause(.03)
Panel_com('set_funcy_freq',50);pause(.03)   % Y pos func not used
Panel_com('set_funcx_freq',samp_rate);pause(.03)

 % I do this in real experiments, it is not needed here though
Panel_com('set_ao',[3,5*(32767/10)]);

Panel_com('start');
%Panel_com('stop');

%% Record the daq signal for the x channel and the ao channel
% The analog input device used as the data acquisition device
testing_dir = 'C:\Users\holtzs\Desktop';
daq_handle = analoginput('nidaq','Dev1');
addchannel(daq_handle,[3 6],{'x_pos','ao'});            
daq_file = fullfile(testing_dir,'data.daq');
set(daq_handle,'LoggingMode','Disk','LogFileName',daq_file,'SampleRate',10000);
set(daq_handle,'SamplesPerTrigger',Inf);

    %%
    start(daq_handle)
    %%
    stop(daq_handle)